/* 
 * Copyright (C) 2017 Christophe Bertrand
 * 
 * Code JavaScript sous licence GNU GPL v3
 * 
 */

var fs = require('fs');
var path = require('path');
var ajv = new require('ajv')();

var o, 
	nb = 0,
	data = [],
	fileName = 'data.json';
	
if(!process.argv[2])throw 'Le nom du répertoire n\'a pas été indiqué.';
if(process.argv[3])fileName = process.argv[3];
if(path.extname(fileName) !== '.json')fileName+='.json';

var schema = JSON.parse(fs.readFileSync('schema.json','utf-8'));

fs.readdirSync(process.argv[2]).forEach(
	function(f){
		if(path.extname(f) === '.json'){
			o = fs.readFileSync(path.join(process.argv[2],f),'utf-8');
			try{
				console.log('-------\nFichier : '+f);
				o = JSON.parse(o);
				if(!ajv.validate(schema,o))throw 'Non valide';
				o.data.forEach(function(d){data.push(d);});
				nb++;
				console.log('OK');
			}catch(e){
				console.log(e);	
			}	
		}
	}
);

console.log('-------\n'+nb+' fichier(s) valide(s)\n'+data.length+' objets de données');
fs.writeFileSync(fileName,JSON.stringify(data,null,4));

