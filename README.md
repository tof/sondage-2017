# Sondage

> Ressources pour la collecte et le stockage au format JSON des données d'un sondage.

## Questionnaire

Le questionnaire a été imaginé par des élèves d'une classe de seconde en cours de S.E.S.. Il comporte douze questions qui portent sur l'utilisation des écrans et des téléphones portables.

## Jeux de données

Les données sont stockées au format **JSON**. Les différents fichiers réalisés par les élèves lors de la saisie (un par élève) figurent dans le répertoire **contributions**.

Le fichier **schema.json** est utilisé pour valider les données saisies, il contient la description du schéma des données décidé collégialement par les élèves. 

Le fichier **data.json** contient le jeu de données complet obtenu par fusion de toutes les données des fichiers des élèves.

## Script de fusion

Le script de fusion permet de valider et rassembler les données dans un seul fichier.

__Prérequis__

**Node.js** doit être installé sur votre ordinateur. Vérifiez dans le script shell (**fusion.sh** ou **fusion.bat**) que le chemin et le nom de l'exécutable de node correspondent à votre installation.

__Usage__

```
Linux :
./fusion.sh REPERTOIRE [DEST]

Windows :
fusion.bat REPERTOIRE [DEST]
```

- REPERTOIRE : nom du répertoire contenant les fichiers à fusionner.
- DEST : nom du fichier où sera enregistré la fusion (par défaut **data.json**).

__Exemple__

```
fusion.bat contributions
```

## Crédits

__Ajv: Another JSON Schema Validator :__
- http://epoberezkin.github.io/ajv/

## Licences

__Script de fusion :__
>Copyright (C) 2017 Christophe Bertrand
>Code JavaScript sous licence GNU GPL v3

__Jeu de données :__
> Les données du sondage sont publiées sous la licence Open Data Commons PDDL (Public Domain Dedication and License) ce qui vous donne la possibilité d’utiliser, de copier, de modifier, et de redistribuer ces données sans aucune restriction.

